package main

import (
	"fmt"
	"os"

	"github.com/pmezard/go-difflib/difflib"
)

func main() {
	diff := difflib.UnifiedDiff{
		A:        []string{"one\n", "two\n", "three\n"},
		B:        []string{"four\n", "five\n", "three\n"},
		FromFile: "a.txt",
		ToFile:   "b.txt",
		Context:  3,
	}

	diffString, err := difflib.GetUnifiedDiffString(diff)

	if err != nil {
		fmt.Printf("And error occurred while generating the diff: %v", err)
		os.Exit(1)
	}

	fmt.Printf("diffstring: %s\n", diffString)
}
